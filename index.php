<?php

// spl_autoload_register(function($class_name){
//     include $class_name . ".php";
// });

/**
 * Displays all products which database contains
 *
 * @package    index_page
 * @author     Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright  Copyright (c) 2019, Vladimir Simic
 */

require 'shared/header.php';

$database = new Database();
$db_conn = $database->connect();

$product = new Products($db_conn);
$category = new Category($db_conn);
?>

<body class="container">

    <header class="header">
        <h2 class="heading-primary">Product List</h2>

        <span class="divider"></span>

        <div class="link">
            <a href="addproduct_form.php">Add product</a>
        </div>
        
        <div class="header__option">
            <select class="header__option--select">
                <option>Select an option</option>
                <option>Delete all</option>
            </select>
        </div><!-- .header__option -->

        <input class="btn btn-text btn__list" 
               type="submit" 
               value="Apply" 
               name="submit">

    </header><!-- .header -->

    <main id="content" class="main-content">
    
    <!-- Loops through all products and displays on the index page -->
    <?php require "components/show_all_products.php"; ?>
         
    </main><!-- #content .main-content -->



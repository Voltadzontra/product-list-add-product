-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 13, 2019 at 09:30 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb2`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'dvd'),
(2, 'book'),
(3, 'furniture');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

DROP TABLE IF EXISTS `parameters`;
CREATE TABLE IF NOT EXISTS `parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size_GB` varchar(30) DEFAULT NULL,
  `weight_Kg` decimal(3,1) DEFAULT NULL,
  `height_cm` decimal(4,1) DEFAULT NULL,
  `width_cm` decimal(4,1) DEFAULT NULL,
  `length_cm` decimal(4,1) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `size_GB`, `weight_Kg`, `height_cm`, `width_cm`, `length_cm`, `product_id`) VALUES
(1, '4.7', NULL, NULL, NULL, NULL, 1),
(2, '4.7', NULL, NULL, NULL, NULL, 2),
(3, '4.7', NULL, NULL, NULL, NULL, 3),
(4, '4.7', NULL, NULL, NULL, NULL, 4),
(5, NULL, '2.0', NULL, NULL, NULL, 5),
(6, NULL, '0.5', NULL, NULL, NULL, 6),
(7, NULL, '1.0', NULL, NULL, NULL, 7),
(8, NULL, '1.2', NULL, NULL, NULL, 8),
(9, NULL, NULL, '85.0', '100.0', '100.0', 9),
(10, NULL, NULL, '100.0', '40.0', '40.0', 10),
(11, NULL, NULL, '200.0', '65.0', '70.0', 11),
(48, NULL, NULL, '100.0', '200.0', '90.0', 54);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `sku` varchar(12) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `category` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sku`, `price`, `category`) VALUES
(1, 'Verbatim', 'JVC200123', '19.99', 1),
(2, 'Maxtor', 'JVC200124', '23.99', 1),
(3, 'Transcend', 'JVC200125', '20.99', 1),
(4, 'Kingston', 'JVC200126', '17.99', 1),
(5, 'War and Piece', 'GGWP0007', '15.99', 2),
(6, 'Nicolas', 'GGWP0008', '10.99', 2),
(7, 'The Island', 'GGWP0009', '11.99', 2),
(8, 'Limitless', 'GGWP0010', '16.99', 2),
(9, 'Table Rima', 'TR120555', '70.99', 3),
(10, 'Chair Dina', 'TR120556', '55.99', 3),
(11, 'Wardrobe Don', 'TR120557', '105.99', 3),
(54, 'Sofa Kristal', 'TR120558', '225.00', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

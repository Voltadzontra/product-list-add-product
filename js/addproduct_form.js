$(document).ready(
    function () {

        /**
         * If there is any value inserted in any input field which belong to category form,
         * other input fields of category form will be disabled for inserting new values
         * This is for preventing from inserting multiple categories' values at the same time.
         */
        var category_elements = ['#size', '#weight', '.dimension'];

        $.each(
            category_elements, function (index, element_name) {

                $(element_name).on(
                    'input', function () {

                        var affected = category_elements.filter(
                            function (value, index, arr) {

                                return element_name.indexOf(value) < 0;
                                // # '#size'.indexOf('#size') == 0
                                // # '#size'.indexOf('#weight') == -1
                                // # '#size'.indexOf('.dismentsion') == -1
                                // # '#weight'.indexOf('#size') == -1
                                // # '#weight'.index('#weight') == 0
                            

                            }
                        );

                        if ($(element_name).val().length && $(element_name).val() >= 0) {

                            $.each(
                                affected, function (index, aff) {

                                    $(aff).prop('disabled', true);

                                }
                            );

                        } else{

                            $.each(
                                affected, function (index, aff) {

                                    $(aff).prop('disabled', false);

                                }
                            );
                        }
                    }
                );
            }
        );


        /**
         * Use inputs_wrapper class to initally hide all category forms.
         * When using drop down category menu, category which is currently selected will show its own form.
         */
        function toggleParameterInputs()
        {

            $('.inputs_wrapper').hide();

            category_id = $('select#category_id_select').find(':selected').text().trim().toLowerCase();

            input_id = '#' + category_id + '_inputs_wrapper';

            $(input_id).show();

        }

        toggleParameterInputs();

        $('select#category_id_select').on(
            'change', function () {

                toggleParameterInputs();
    
            }
        ); 
    }
);




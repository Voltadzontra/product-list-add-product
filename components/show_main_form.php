<?php            
            echo "<label for='sku'>SKU: <span>*</span></label>";

            if (isset($_GET['sku'])) {

                $sku = $_GET['sku'];

                echo '<input type="text"
                             name="sku"
                             id="sku"
                             value="'.htmlspecialchars($sku).'"><br />';

            } else {

                echo '<input type="text" name="sku" id="sku"><br />';

            }

                echo "<label for='name'>Name: <span>*</span></label>";

            if (isset($_GET['name'])) {

                $name = $_GET['name'];

                    echo '<input type="text"
                                 name="name"
                                 id="name"
                                 value="'.htmlspecialchars($name).'"><br />';

            } else {

                echo '<input type="text" name="name" id="name"><br />';

            }

                echo "<label for='price'>Price(&euro;): <span>*</span></label>";

            if (isset($_GET['price'])) {

                $price = $_GET['price'];

                echo '<input type="text"
                             name="price"
                             id="price"
                             value="'.htmlspecialchars($price).'">';

            } else {

                echo '<input type="text"
                             name="price"
                             id="price"
                             placeholder="00.00"><br />';

            }
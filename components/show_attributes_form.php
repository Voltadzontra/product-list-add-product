<?php
try {

      $categories = $category->read();

      foreach ($categories as $cat) {
          ?>
          <option value="<?php echo $cat['id']; ?>" 

              <?php if (isset($_GET['category']) && ($_GET['category'] == $cat['id'])) { 
                  echo 'selected'; 
              } ?>>

              <?php echo ucfirst($cat['name']); ?>

          </option>
          <?php
      }
  } catch (PDOException $error) {

      echo "Cannot add a product: " . $error->getMessage();
                        
  }

  $database->disconnect();
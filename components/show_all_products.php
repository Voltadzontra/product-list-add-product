<?php
    /**
     * Displays all products on index page
     *
     * This block of code lists out all products from the database
     * by using the $sql variable as a parameter in the
     * prepared statement. The main part of the code is
     * while condition which fetches all rows from the database
     * as long as we have products.
     * Once there is a database connection problem,
     * it throws an error.
     * 
     * @var    $sql string
     * @var    $query object
     * @var    $row array
     * @throws PDOException if there is a database connection problem
     */
    try{
        $sql="SELECT products.id, products.sku, products.name, products.price,
                     categories.name AS categories_name, parameters.size_GB,
                     parameters.weight_Kg, parameters.height_cm,
                     parameters.width_cm, parameters.length_cm
              FROM products
              LEFT JOIN
                categories ON products.category = categories.id
              LEFT JOIN
                parameters ON parameters.product_id = products.id";

        $query=$db_conn->prepare($sql);

        $query->execute();

        while ($row = $query->fetch()) {
            ?>
           <div class="card">

                <div class="card__checkbox">
                    <form action = "#" method = "post">
                           <label for="<?php echo 'checkbox' . $row['id'];?>" 
                                  class="checkboxContainer">
                               <input type="checkbox"
                                      id="<?php echo 'checkbox' . $row['id'];?>"
                                      name="checkbox"
                                      value="checkbox"> 
                               <span class="checkmark"></span>
                           </label>
                    </form>
                </div><!-- .card__checkbox -->
                
                   <div class="card__details">
                    <ul class="<?php echo $row['categories_name']; ?>">
                        <li>SKU: 
                            <span class="card__details--data">
                                <?php echo $row['sku']; ?>
                            </span>
                        </li>
                        <li>Name: 
                            <span class="card__details--data">
                                <?php echo $row['name']; ?>  
                            </span>
                        </li>
                        <li>Price:
                            <span class="card__details--data">
                                <?php echo $row['price']; ?>  
                            </span>
                        </li>
                        <li class="dvd_parameters">Size:
                            <span class="card__details--data">
                                <?php echo $row['size_GB'] . " GB"; ?>
                            </span>
                            <div class="card__picture card__picture--1">&nbsp;</div>
                        </li>
                        <li class="book_parameters">Weight: 
                            <span class="card__details--data">
                                <?php echo $row['weight_Kg'] . " Kg"; ?>
                            </span>
                            <div class="card__picture card__picture--2">&nbsp;</div>
                        </li>
                        <li class="furniture_parameters">Dimensions: 
                            <span class="card__details--data">
                                <?php echo $row['height_cm'] . ' x ' . $row['width_cm'] . ' x ' . $row['length_cm']; ?>
                            </span>
                            <div class="card__picture card__picture--3">&nbsp;</div>
                        </li>
                    </ul>
                </div><!-- .card__details -->
            </div><!-- .card -->

            <?php
        }
    } catch(PDOException $error) {

        echo "Connection problem: " . $error->getMessage();

    }

          $database->disconnect();
    ?>
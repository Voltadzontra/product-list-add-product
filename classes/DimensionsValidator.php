<?php
include_once 'classes/FieldValidatorInterface.php';

/**
 * Checks a furniture Dimensions field
 *
 * If there is/are either field/s left empty or
 * wrong input was made by user,
 * there will be an error message displayed.
 * Input must be either a whole number or
 * a decimal. Alphabetic characters are
 * not allowed.
 * All three fields must be filled out.
 */
class DimensionsValidator implements FieldValidator
{

    public function validate()
    {

        $height = htmlspecialchars(strip_tags($_POST['height']));

        $width = htmlspecialchars(strip_tags($_POST['width']));

        $length = htmlspecialchars(strip_tags($_POST['length']));

        $height_errors = !preg_match('/^[0-9]+(\.[0-9]+)?$/', $height) ? ['height_validation' => 'invalidchar'] : [];

        $width_errors = !preg_match('/^[0-9]+(\.[0-9]+)?$/', $width) ? ['width_validation' => 'invalidchar'] : [];

        $length_errors = !preg_match('/^[0-9]+(\.[0-9]+)?$/', $length) ? ['length_validation' => 'invalidchar'] : [];

        return array_merge(
            $height_errors,
            $width_errors,
            $length_errors
        );

    }

    public function errors()
    {

        $height = isset($_GET['height_validation']) ? $_GET['height_validation'] : null;

        $width = isset($_GET['width_validation']) ? $_GET['width_validation'] : null;

        $length = isset($_GET['length_validation']) ? $_GET['length_validation'] : null;

        if ($height == 'invalidchar' || $width == 'invalidchar' || $length == 'invalidchar') {

            return 'All furniture fields should contain
                    either a whole positive number or a decimal!';

        }
    }
}
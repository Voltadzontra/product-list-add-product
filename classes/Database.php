<?php

class Database
{
    /**
     * @var $options array
     */
    private $options = array(

       PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
       PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC

                );
    
    /**
     * @var $server string
     */
    private $server = "mysql:host=localhost;dbname=scandiweb2";
    
    /**
     * @var $username string
     */
    private $username = 'root';
    
    /**
     * @var $password string
     */
    private $password = '';

    /**
     * @var $conn object
     */
    private $conn;

    /** 
     * @throws PDOException if there is a problem with
     *         any parameter in the PDO object
     */
    public function connect()
    {
        try{

            $this->conn = new PDO($this->server, $this->username, $this->password, $this->options);
                
            return $this->conn;
                
        } catch(PDOException $error) {

            echo "Failed to connect: " . $error->getMessage();

        }
    }

    public function disconnect()
    {

        $this->conn = null;
            
    }
}

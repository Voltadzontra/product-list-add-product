<?php


// include_once 'classes/DimensionsValidator.php';
// include_once 'classes/NameValidator.php';
// include_once 'classes/PriceValidator.php';
// include_once 'classes/SizeValidator.php';
// include_once 'classes/SkuValidator.php';
// include_once 'classes/WeightValidator.php';
/**
 * This array collects all classes and makes new instances 
 */
$error_objects = [
    new SkuValidator(),
    new NameValidator(),
    new PriceValidator(),
    new SizeValidator(),
    new WeightValidator(),
    new DimensionsValidator()
];

/**
 * ErrorsDisplay class helps to display all possible errors
 *
 * @package Field_Validator
 * @author  Vladimir Simic <vladimir.d.simic@gmail.com>
 */
class ErrorsDisplay
{
    /**
     * Displays all possible errors made in the form
     *
     * A display() method echos error messages
     * when one or more errors are occured.
     *
     * @param  $error_objects array
     * @var    $error string
     *
     * @return void
     */
    public function display($error_objects)
    {
        
        foreach ($error_objects as $error_object) {

            $error = $error_object->errors();

            if ($error) {

                echo "<p>".$error."</p>";
                
            }
        }
    }
}
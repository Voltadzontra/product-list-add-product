<?php
// include_once 'classes/FieldValidatorInterface.php';
/**
 * Checks Price field
 *
 * If there is either field left empty or
 * wrong input was made by user,
 * there will be an error message displayed.
 * Every price must be written like a decimal
 * e.g. 1.0, 23.2, 12.13
 * Alphabetical characters are not allowed.
 */
class PriceValidator implements FieldValidator
{

    public function validate()
    {

        $price = htmlspecialchars(strip_tags($_POST['price']));

        if (empty($price)) {

            return ['price_validation' => 'empty'];

          /**
           * This preg_match makes sure that only inputs which are 
           * whole numbers or decimals are acceptable
           *
           * @param mixed $price
           */
        } elseif (!preg_match('/[0-9]+\.([0-9]+)/', $price)) {

            return ['price_validation' => 'invalidprice'];

        } else {

            return [];

        }

    }

    public function errors()
    {

        if (isset($_GET['price_validation'])) {

            $price = $_GET['price_validation'];

            if ($price == 'invalidprice') {

                return 'You need to input decimals
                        in price field in this format XX.XX';

            } elseif ($price == 'empty') {

                return 'You cannot leave empty price field in the form!';

            }
        }
    }
}
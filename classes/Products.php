<?php
// require_once "classes/FieldValidatorInterface.php";

/**
 * Products contains a class, properties, constructor and
 * a method for making a new product
 *
 * @package   Products
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */

/**
 * Products class has properties, constructor and a method
 * for making a new product
 *
 * @package   Products
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */
class Products
{
    /**
     * This variable contains a PDO object and
     * it is crucial for connecting to the database
     *
     * @var $conn object 
     */ 
    private $conn;     
     
    /**
     * Connects to the database
     *
     * @param $db_conn object
     *
     * @return void
     */ 
    public function __construct($db_conn)
    {

        $this->conn = $db_conn;

    }
 
    /**
     * Creates a new product if all conditions are fulfilled
     *
     * A create() method is made of two big sections.
     * First section is related to the data validation, and
     * the second part is related to using all necessary data
     * for making a new product and send it to the database.
     * If the first part od the method is successfuly passed
     * without any errors, than the program will begin
     * transaction and make a product.
     *
     * @package   Products
     * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
     * @copyright Copyright (c) 2019, Vladimir Simic
     * @var       $form_values array
     * @var       $common_validators array
     * @var       $category_validators array
     * @var       $all_validators array
     * @var       $apply_validate array
     *
     * @return void
     */
    public function create()
    {
        
        $form_values = array(
            'sku' => htmlspecialchars(strip_tags(strtoupper($_POST['sku']))),
            'name' => htmlspecialchars(strip_tags($_POST['name'])),
            'price' => htmlspecialchars(strip_tags($_POST['price'])),
            'category' => htmlspecialchars(strip_tags($_POST['category'])),
            'size' => htmlspecialchars(strip_tags($_POST['size'])),
            'weight' => htmlspecialchars(strip_tags($_POST['weight'])),
            'height' => htmlspecialchars(strip_tags($_POST['height'])),
            'width' => htmlspecialchars(strip_tags($_POST['width'])),
            'length' => htmlspecialchars(strip_tags($_POST['length']))
        );
        
        /**
         * An array of objects created for all type of products
         *
         * This array contains all input field validators
         * which are common for all products not matter
         * what the type of the product is.
         */
        $common_validators = [
            new SkuValidator(),
            new NameValidator(),
            new PriceValidator()
        ];
        
        /**
         * An array of objects created for specific type of products
         *
         * This array contains input field validators which are not
         * common for all products. For products such as books
         * the typical field which need to be validated is weight,
         * for discs is size field, and for furniture are three
         * dimensions fields.
         */
        $category_validators = [
            1 => new SizeValidator(),
            2 => new WeightValidator(),
            3 => new DimensionsValidator() 
        ];

        /**
         * Merge common validator fields with specific type of fields
         */
        $all_validators = array_merge(
            $common_validators,
            [$category_validators[$form_values['category']]]
        );
        
        /**
         * @param $validator object
         *
         * @return object
         */
        $apply_validate = function ($validator) {

            return $validator->validate();

        };
        
        /**
         * This variable holds array whose result is a return of
         * applying a callback function to the $all_validators array
         *
         * @var @mapped_errors array
         *
         * @param $apply_validate function
         * @param $all_validators array
         */
        $mapped_errors = array_map($apply_validate, $all_validators);
        
        /**
         * This variable holds merging array function with various
         * number of arguments. Knowing that depending on the
         * product category type we could have either one input field,
         * such as size or weight, or three input fields such as
         * height, width and length at the same time. The splat operator
         * provides that either a product with one category parameter or
         * three category parameters will be taken into consideration
         * and the new product will be made.
         * 
         * @var $validation_errors array
         *
         * @param $mapped_errors array
         * @param $all_validators array
         */
        $validation_errors = array_merge(... $mapped_errors);

        if (!empty($validation_errors)) {

            $form_values_query = http_build_query($form_values);

            $validation_errors_query = http_build_query($validation_errors);

            header('Location: addproduct_form.php?'.$form_values_query.'&'.$validation_errors_query);

            exit();
                             
        } else {
            /**
             * This block of code uses all data
             * which are needed for making a new product.
             * While making a new product, program takes certain
             * parameters and writes them in two separate tables
             * in the database. The transaction provides
             * inserting the data into both tables in the database.
             * 
             * @var $sql string
             * @var $result string
             * @var $sql1 string
             * @var $result1 string
             *
             * @param $sql string
             * @param $sql1 string
             *
             * @throws PDOException if there is a problem with
             *         inserting data in a database
             */
            try{
                $this->conn->beginTransaction();

                $sql = "INSERT INTO products (sku, name, price, category)
                        VALUES (:sku, :name, :price, :category)";
                    
                $result = $this->conn->prepare($sql);
                    
                // bind values 
                $result->bindParam(':sku', $form_values['sku']);
                $result->bindParam(':name', $form_values['name']);
                $result->bindParam(':price', $form_values['price']);
                $result->bindParam(':category', $form_values['category']);

            
                $result->execute();

                $product_id = $this->conn->lastInsertId();


                $sql1= "INSERT INTO parameters (size_GB, weight_Kg, height_cm,
                                    width_cm, length_cm, product_id)
                        VALUES (:size_GB, :weight_Kg, :height_cm, :width_cm,
                               :length_cm, :product_id)";

                $result1 = $this->conn->prepare($sql1);

                $result1->bindValue(
                    ":size_GB",
                    !empty($form_values['size']) ? $form_values['size'] : null
                );

                $result1->bindValue(
                    ":weight_Kg",
                    !empty($form_values['weight']) ? $form_values['weight'] : null
                );

                $result1->bindValue(
                    ":height_cm",
                    !empty($form_values['height']) ? $form_values['height'] : null
                );

                $result1->bindValue(
                    ":width_cm",
                    !empty($form_values['width']) ? $form_values['width'] : null
                );

                $result1->bindValue(
                    ":length_cm",
                    !empty($form_values['length']) ? $form_values['length'] : null
                );
                
                $result1->bindParam(':product_id', $product_id);

                $result1->execute();

                $this->conn->commit();

                header('Location: index.php');

            } catch(PDOException $error){

                $this->conn->rollBack();

                echo $error->getMessage();

            }
        }
    }
}


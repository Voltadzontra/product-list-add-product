<?php
/**
 * Category
 *
 * @package   Product_Category
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */

/**
 * Product categories class
 *
 * Category class uses its own properties and methods to
 * query through categories in the database, fetch all
 * results from the database, and display them in
 * a drop down menu. The dislayed results offers a choice
 * to the user among DVD, Book and Furniture category.
 *
 * @package   Product_Category
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */
class Category
{
    /**
     * An ID of the product used for making a query
     * whose result displays one of three product categories
     * (Book, Disc and Furniture) in the drop down menu
     * depending on the ID of each product
     *
     * @var $id int
     */
    public $id;
    
    /**
     * A name of the product in the database
     *
     * @var $name string
     */
    public $name;

    /**
     * Connects to the database
     *
     * @param  $db_conn object
     *
     * @return void
     */
    public function __construct($db_conn)
    {

        $this->conn = $db_conn;

    }

    /**
     * Selects the drop down list
     *
     * This function contains the $sql variable which is a parameter in the
     * prepared statement. It fetches all category products
     * whose result is assigned to the $categories variable and
     * returns the string.
     *
     * @var    $sql string
     * @var    $result object
     * @return string $categories
     */
    public function read()
    {
        
        $sql = "SELECT
                    id, name
                FROM
                    categories
                ORDER BY
                    name";                      

        $result = $this->conn->prepare($sql);

        $result->execute();
        
        $categories = $result->fetchAll();
 
        return $categories;

    }

}

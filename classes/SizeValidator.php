<?php
// include_once 'classes/FieldValidatorInterface.php';
/**
 * Checks a disc Size field
 *
 * If there is either field left empty or
 * wrong input was made by user,
 * there will be an error message displayed.
 * Input must be either a whole number or
 * a decimal. Alphabetic characters are
 * not allowed.
 */
class SizeValidator implements FieldValidator
{

    public function validate()
    {

        $size = htmlspecialchars(strip_tags($_POST['size']));

        if (empty($size)) {

            return ['size_validation' => 'empty'];

        } elseif (!preg_match('/^[1-9]+(\.[0-9]+)?$/', $size)) {

            return ['size_validation' => 'invalidchar'];

        } else {

            return [];
            
        }

    }

    public function errors()
    {

        if (isset($_GET['size_validation'])) {

            $size = $_GET['size_validation'];

            if ($size == 'invalidchar') {

                return 'A size field should contain
                        either a whole positive number or a decimal!';

            } elseif ($size == 'empty') {

                return 'You cannot leave empty size field in the form!';

            }
        }
    }
}
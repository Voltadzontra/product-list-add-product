<?php
/**
 * FieldValidator checks all input fields in the form
 *
 * This validator checks if the user left empty input field,
 * prevents SQL Injections, or if the user made a typo
 * and tried to input unappropriate characters or
 * too short inputs for certain fields.
 *
 * @package   Field_Validator
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */

/**
 * ProductValidator interface holds signs of
 * validate and error methods
 *
 * @package   Field_Validator
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */
interface FieldValidator
{
    /**
     * Validates the form inputs made by the user
     *
     * This method only checks the input made by user.
     * If any input is invalid, the method will return
     * an associative array - key/value pair.
     * Depending on the class which contains the validate() method,
     * the name of key will be related to that class.
     * If there is an error, the value will be pointing on
     * a type of the error. If there is no error,
     * it will return an empty array.
     *
     * @return array
     */
    public function validate();

    /**
     * Recognizes the type of an error
     *
     * This method is not read if there is no error.
     * If there is an error, the method will return a message
     * related to the type of the error.
     *
     * @return string
     */
    public function errors();

}
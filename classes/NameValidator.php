<?php
// include_once 'classes/FieldValidatorInterface.php';
/**
 * Checks Name field
 *
 * If there is field left empty
 * an error message will be displayed.
 * There is no any limits for name length
 * or for the character type.
 */
class NameValidator implements FieldValidator
{

    public function validate()
    {

        $name = htmlspecialchars(strip_tags($_POST['name']));

        if (empty($name)) {

            return ['name_validation' => 'empty'];

        } else {

            return [];

        }

    }

    public function errors()
    {

        if (isset($_GET['name_validation'])) {

            $name = $_GET['name_validation'];

            if ($name == 'empty') {

                return 'You cannot leave empty name field in the form!';

            }
        }
    }
}
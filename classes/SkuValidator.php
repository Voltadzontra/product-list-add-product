<?php
// include_once 'classes/FieldValidatorInterface.php';
/**
 * Checks SKU field
 *
 * If there is either field left empty or
 * wrong input was made by user,
 * an error message will be displayed.
 * Input must be at least eight characters long
 * and it can contain both numbers and letters.
 */
class SkuValidator implements FieldValidator
{

    public function validate()
    {

        $sku = htmlspecialchars(strip_tags($_POST['sku']));

        if (empty($sku)) {

            return ['sku_validation' => 'empty'];

        } elseif (!preg_match('/^([a-zA-Z0-9_-]){8,}$/', $sku)) {

            return ['sku_validation' => 'invalidchars'];

        } else {

            return [];

        }

    }

    public function errors()
    {

        if (isset($_GET['sku_validation'])) {

            $sku = $_GET['sku_validation'];

            if ($sku == 'invalidchars') {

                return 'Field SKU must contain at least 8 characters.';

            } elseif ($sku == 'empty') {

                return 'You cannot leave empty SKU field in the form!';

            }
        }
    }
}
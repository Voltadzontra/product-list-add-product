<?php
// include_once 'classes/FieldValidatorInterface.php';
/**
 * Checks a book Weight field
 *
 * If there is either field left empty or
 * wrong input was made by user,
 * there will be an error message displayed.
 * Input must be either a whole number or
 * a decimal. Alphabetic characters are
 * not allowed.
 */
class WeightValidator implements FieldValidator
{

    public function validate()
    {
        
        $weight = htmlspecialchars(strip_tags($_POST['weight']));

        if (empty($weight)) {

            return ['weight_validation' => 'empty'];

        } elseif (!preg_match('/^[0-9]+(\.[0-9]+)?$/', $weight)) {

            return ['weight_validation' => 'invalidchar'];

        } else {

            return [];
            
        }

    }

    public function errors()
    {

        if (isset($_GET['weight_validation'])) {

            $weight = $_GET['weight_validation'];

            if ($weight == 'invalidchar') {

                return 'A weight field should contain
                        either a whole positive number or a decimal!';

            } elseif ($weight == 'empty') {

                return 'You cannot leave empty weight field in the form!';

            }
        }
    }
}
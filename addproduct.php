<?php
    require "shared/header.php";
?>

<?php
if (isset($_POST['insert'])) {
    $database = new Database();
    $db_conn = $database->connect();

    try{

        $product = new Products($db_conn);
        $product->create();

    } catch(PDOException $error) {
        echo "Cannot add a product: " . $error->getMessage();
    }

    $database->disconnect();
}    
?>

<?php
spl_autoload_register(function($class_name){
  if(file_exists("classes/" . $class_name . ".php")){

    include "classes/" . $class_name . ".php";

  } else if(file_exists("shared/" . $class_name . ".php")){

    include "shared/" . $class_name . ".php";

  } else if (file_exists("components/" . $class_name . ".php")) {

    include "components/" . $class_name . ".php";

  }
    
    // include "components/" . $class_name . ".php";
    // include "shared/" . $class_name . ".php";
});

// require "shared/header.php";
// require_once 'classes/FieldValidatorInterface.php';
// require_once 'classes/ErrorsDisplay.php';

/**
 * Form for adding new products to the product list
 *
 * @category  ProductForm
 * @package   Product_Form
 * @author    Vladimir Simic <vladimir.d.simic@gmail.com>
 * @copyright Copyright (c) 2019, Vladimir Simic
 */

$database = new Database();
$db_conn = $database->connect();

$products = new Products($db_conn);
$category = new Category($db_conn);

?>

<body class="container-form">
    <header class="header">

        <h2 class="heading-primary">Product Add</h2>

        <span class="divider"></span>

        <div class="link">
            <a href="index.php">Product list</a>
        </div>

        <script type="text/javascript" src="js/addproduct_form.js"></script>

  </header>
  <main class="main-form-content">
    <form class="main-form" 
          name="main-form" 
          action="addproduct.php" 
          method="post">
        <div class="main-form__form_content" id="form-content">
        
            <!-- Show main form common to all products -->
            <?php require "components/show_main_form.php"; ?>

            <input class='btn btn-text btn__add'
                   type='submit'
                   name='insert'
                   value='Save'>
        </div><!--.main-form__form_content-->

        <div class="switch-type">
            <select id='category_id_select'
                    name='category'
                    class='switch-type__option--select'>

                <!-- Show attributes form which shows attributes
                    related to a different product -->
                <?php require "components/show_attributes_form.php"; ?>
                
            </select>
        </div><!--.switch-type-->

        <p class="warning">
            <strong>Note that all fields marked with * are mandatory!</strong>
        </p>

        <div class="form-changer">
            <div id='dvd_inputs_wrapper' class='inputs_wrapper'>
                <div class="form-changer__size">
                    <label for="size">Size: <span>*</span></label>
                <input type="text"
                       name="size"
                       id="size"
                       value="<?php
                        if (isset($_GET['size'])) { 
                            echo $_GET['size'];
                        } 
                        ?>">
                    <p class="form-changer__size--description">
                        &rsaquo; Please, provide the size of the disc in
                          <strong>MB</strong>.<br />
                        &rsaquo; Decimals are allowed.
                    </p>
                </div><!--.form-changer__size-->
            </div><!--.inputs_wrapper-->
            
            <div id='book_inputs_wrapper' class='inputs_wrapper'>
                <div class='form-changer__weight'>
                <label for="size">Weight: <span>*</span></label>
                <input type="text"
                       name="weight"
                       id="weight"
                       value="<?php
                        if (isset($_GET['weight'])) {
                            echo $_GET['weight'];
                        }
                        ?>">
                    <p class='form-changer__weight--description'>
                        &rsaquo; Please, provide the weight of the book in
                          <strong>Kg</strong>.<br />
                        &rsaquo; Decimals are allowed.
                    </p>
                </div><!--.form-changer__weight-->
            </div><!--.inputs_wrapper-->
            

             <div id='furniture_inputs_wrapper' class='inputs_wrapper'>
                <div class="form-changer__dimension">
                    <label for="dimension">Dimension:<span>*</span></label>
                    <input type="text"
                           name="height"
                           class="dimension"
                           placeholder="H"
                           value="<?php
                            if (isset($_GET['height'])) {
                                echo $_GET['height'];
                            }
                            ?>">
                    <input type="text"
                           name="width"
                           class="dimension"
                           placeholder="W"
                           value="<?php
                            if (isset($_GET['width'])) {
                                echo $_GET['width'];
                            }
                            ?>">
                    <input type="text"
                           name="length"
                           class="dimension"
                           placeholder="L"
                           value="<?php
                            if (isset($_GET['length'])) {
                                echo $_GET['length'];
                            }
                            ?>">
                    <p class="form-changer__dimension--description">
                        &rsaquo; Please, provide dimensions of the furniture in
                          <strong>centimeters</strong>.<br />
                        &rsaquo; Decimals are allowed.
                    </p>
                </div><!--.form-changer__dimension-->
            </div><!--.inputs_wrapper-->
        </div><!--.form-changer-->
    </form>
    
    <div class="error-handler">
        <div class="error-handler__error">
            <?php
                $allErr = new ErrorsDisplay();
                $allErr->display($error_objects);
            ?>
        </span>
    </div><!--.error-handler-->
  </main>
</body>
